<?php

ini_set('memory_limit', '4096M');
ini_set('auto_detect_line_endings', true);

include 'files.php';

function add_row($row,$ohandle) {
	
	$zip3	=	substr($row[3],0,3);
			
	$out	=	array(
		$row[1],
		'Available',
		'',
		'',
		$row[2],
		$row[3],
		$zip3,
		$row[4],
		$row[5],
		$row[6],
		$row[9],
		$row[10],
		$row[17]
	);
	
	fputcsv($ohandle,$out);
}

function add_state($state,$ohandle,$border_zips = false) {
		
	$ifile	=	'csvs/'. $state .'.csv';
	$ihandle	=	fopen($ifile,'r');
	
	$count	=	0;
	
	while( !feof($ihandle) ) {

		$row	=	fgetcsv($ihandle);
		
		if( $count != 0 ) {
	
			$zip3	=	substr($row[3],0,3);
		
			if( $border_zips && in_array($zip3, $border_zips) ) {
				add_row($row,$ohandle);
			}
			else if( !$border_zips ) {
				add_row($row,$ohandle);
			}
			
		}
		
		$count++;
		
	}
	
	fclose($ihandle);
	
}

$totalcount	=	0;

foreach( $kml_files as $key	=>	$values ) {
	
	$ofile	=	'output-'. $key .'.csv';
	$ohandle	=	fopen($ofile,'w');
	
	$headers	=	array(
		'Route ID',
		'Availability',
		'Company',
		'Segment',
		'ZipCRID',
		'Zip Code',
		'Zip3',
		'Route Code',
		'Route Name',
		'State',
		'lat',
		'lon',
		'geometry'
	);
	
	fputcsv($ohandle,$headers);
	
	foreach( $values['states'] as $state ) {
		add_state($state,$ohandle);
		echo "$state added \n";
	}
	
	foreach( $values['border_states'] as $state ) {
		add_state($state,$ohandle,$values['border_zips']);
		echo "$state added \n";
	}
	
	fclose($ohandle);
	
}

echo '<h2>Total Count: '. $totalcount .'</h2>';

?>