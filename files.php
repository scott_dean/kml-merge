<?php

$kml_files	=	array(
	'NewEngland'	=>	array(
		'states'	=>	array( 'MA', 'CT', 'NH', 'VT', 'NY', 'ME', 'RI' ),
		'border_states'	=>	array( 'PA', 'NJ' ),
		'border_zips'	=>	array(	/* New Jersey */	'074', '076', '075', '070', '071', '072', '079', '088', '077', '089', '085', '087',
									/* Pennsylvania */	'183', '184', '188', '169', '167', '163', '164', '165' )
	),
	'DC-MD-WV-VA'	=>	array(
		'states'	=>	array( 'MD', 'WV', 'VA', 'DC' ),
		'border_states'	=>	array( 'OH', 'KY', 'TN', 'NC', 'PA', 'DE' ),
		'border_zips'	=>	array(	/* Ohio */			'439', '457', '456',
									/* Pennsylvania */	'153', '154', '155', '172', '173', '193',
									/* N. Carolina */	'286', '270', '272', '273', '275', '278', '279',
									/* Tennessee */		'376', '377', '378',
									/* Delaware */		'199', '197',
									/* Kentucky */		'411', '412', '415', '418', '408', '409' )
	),
	'DE-NJ-PA'	=>	array(
		'states'	=>	array( 'PA', 'NJ', 'DE' ),
		'border_states'	=>	array( 'OH', 'WV', 'MD', 'NY' ),
		'border_zips'	=>	array(	/* New York */		'103', '112', '100', '113', '104', '107', '105', '106', '105', '109', '127', '137', '138', '139', '148', '149', '147', '140', '101', '111',
									/* Ohio */			'440', '444', '445', '439', 
									/* Maryland */		'215', '217', '216', '211', '210', '219', '218',
									/* W. Virginia */	'260', '265' )
	),
	'IN-MI-OH'	=>	array(
		'states'	=>	array( 'MI', 'IN', 'OH' ),
		'border_states'	=>	array( 'WI', 'IL', 'KY', 'WV', 'PA' ),
		'border_zips'	=>	array(	/* Wisconsin */		'541', '545',
									/* Pennsylvania */	'164', '161', '150', '153',
									/* Illinois */		'606', '604', '609', '618', '619', '624', '628',
									/* Kentucky */		'424', '423', '401', '402', '400', '410', '411', 
									/* W. Virginia */	'257', '255', '252', '261', '260', )
	),
	'KY-TN'	=>	array(
		'states'	=>	array( 'KY', 'TN' ),
		'border_states'	=>	array( 'OH', 'IN', 'IL', 'MO', 'AR', 'MS', 'AL', 'GA', 'NC', 'VA', 'WV' ),
		'border_zips'	=>	array(	/* Alabama */		'357', '356',
									/* Ohio */			'456', '451', '452', '450', 
									/* Virginia */		'246', '242',
									/* W. Virginia */	'255', '257', '256',
									/* N. Carolina */	'286', '287', '289',
									/* Missouri */		'638',
									/* Florida */		'325', '324', '323', '320', '322',
									/* Illinois */		'629', '477', 
									/* Indiana */		'476', '477', '475', '471', '472', '470',
									/* Arkansas */		'723',
									/* Georgia */		'305', '307',
									/* Mississippi */	'388', '386', )
	),
	'NC-SC'	=>	array(
		'states'	=>	array( 'NC', 'SC' ),
		'border_states'	=>	array( 'VA', 'TN', 'GA' ),
		'border_zips'	=>	array(	/* Virginia */		'234', '233', '238', '239', '245', '241', '240', '243',
									/* Tennessee */		'376', '377', '378', '373', 
									/* Georgia */		'305', '306', '308', '309', '304', '313', '314', )
	),
	'FL-GA'	=>	array(
		'states'	=>	array( 'FL', 'GA' ),
		'border_states'	=>	array( 'AL', 'TN', 'SC', 'NC'  ),
		'border_zips'	=>	array(	/* N. Carolina */	'359', '362', '368', '360', '363', '364', '365',
									/* S. Carolina */	'299', '298', '296',
									/* Tennessee */		'373', '374', '445', '439',
									/* N. Carolina */	'289', '287', )
	),
	'AL-LA-MS'	=>	array(
		'states'	=>	array( 'AL', 'MS', 'LA' ),
		'border_states'	=>	array( 'TX', 'AR', 'TN', 'GA', 'FL' ),
		'border_zips'	=>	array(	/* Texas */		'755', '756', '759', '776',
									/* Florida */	'324', '325',
									/* Tennessee */	'373', '374', '384', '383', '380', '381',
									/* Georgia */	'307', '301', '302', '318', '319', '398',
									/* Arkansas */	'718', '717', '716', '723', '721' )
	),
	'AR-MO'	=>	array(
		'states'	=>	array( 'AR', 'MO' ),
		'border_states'	=>	array( 'KS', 'NE', 'IA', 'IL', 'KY', 'TN', 'MS', 'LA', 'TX', 'OK'  ),
		'border_zips'	=>	array(	/* Kansas */	'660', '661', '662', '667',
									/* Oklahoma */	'743', '749', '747',
									/* Texas */		'755',
									/* Tennessee */	'380', '381',
									/* Mississippi */	'386', '387',
									/* Nebraska */	'684', '683',
									/* Iowa */		'516', '508', '501', '500', '525', '526',
									/* Illinois */	'623', '620', '622', '629',
									/* Kentucky */	'420',
									/* Louisiana */	'710', '712'  )
	),
	'IA-IL-WI'	=>	array(
		'states'	=>	array( 'IL', 'IA', 'WI' ),
		'border_states'	=>	array( 'MO', 'MN', 'NE', 'SD', 'MI', 'IN', 'KY'  ),
		'border_zips'	=>	array(	/* S. Dakota */	'570', '661', '662', '667',
									/* Missouri */	'644', '646', '635', '634', '633', '630', '631', '636', '637', '638',
									/* Nebraska */	'680', '681', '684', '683',
									/* Kentucky */	'420', '424',
									/* Michigan */	'499', '498',
									/* Minnesota */	'550', '557', '558', '550', '559', '560', '561',
									/* Indiana */	'464', '463', '479', '478', '475', '476', '477',  )
	),
	'MN-ND-NE-SD'	=>	array(
		'states'	=>	array( 'MN', 'ND', 'SD', 'NE' ),
		'border_states'	=>	array( 'WI', 'MT', 'WY', 'CO', 'KS', 'IA', 'MO' ),
		'border_zips'	=>	array(	/* Kansas */	'677', '676', '669', '664', '665', '660',
									/* Wisconsin */	'548', '540', '547', '546',
									/* Montana */	'592', '593',
									/* Colorado */	'807',
									/* Missouri */	'644',
									/* Iowa */		'521', '504', '505', '513', '512', '520', '511', '515', '516',
									/* Wyoming */	'827', '822', '820' )
	),
	'CO-KS-OK'	=>	array(
		'states'	=>	array( 'KS', 'OK', 'CO' ),
		'border_states'	=>	array( 'UT', 'WY', 'NE', 'MO', 'AR', 'TX', 'NM' ),
		'border_zips'	=>	array(	/* Utah */		'840', '845',
									/* Wyoming */	'829', '823', '820',
									/* Texas */		'790', '792', '763', '762', '750', '754', '755',
									/* Nebraska */	'691', '690', '689', '683', '684',
									/* Missouri */	'644', '645', '640', '641', '647', '648',
									/* N. Mexico */	'884', '877', '874', '875',
									/* Arkansas */	'727', '729', '719', '718' )
	),
	'TX'	=>	array(
		'states'	=>	array( 'TX' ),
		'border_states'	=>	array( 'NM', 'OK', 'AR', 'LA' ),
		'border_zips'	=>	array(	/* N. Mexico */	'884', '881', '882', '883', '880',
									/* Oklahoma */	'739', '738', '736', '735', '734', '747',
									/* Arkansas */	'718',
									/* Louisiana */	'710', '711', '714', '706' )
	),
	'AZ-NM-UT'	=>	array(
		'states'	=>	array( 'AZ', 'NM', 'UT' ),
		'border_states'	=>	array( 'TX', 'OK', 'CO', 'WY', 'ID', 'NV', 'CA' ),
		'border_zips'	=>	array(	/* California */'923', '922', '882',
									/* Oklahoma */	'739', 
									/* Texas */		'790', '793', '797', '798', '799',
									/* Idaho */		'832', '833',
									/* Colorado */	'816', '815', '814', '813', '811', '810',
									/* Nevada */	'890', '893', '898',
									/* Wyoming */	'831', '829' )
	),
	'CA-NV'	=>	array(
		'states'	=>	array( 'CA', 'NV' ),
		'border_states'	=>	array( 'AZ', 'UT', 'ID', 'OR' ),
		'border_zips'	=>	array(	/* Arizona */	'864', '853',
									/* Utah */		'843', '840', '847', 
									/* Idaho */		'833', '836',
									/* Oregon */	'974', '975', '977', '976', '979' )
	),
	'WA-OR'	=>	array(
		'states'	=>	array( 'WA', 'OR' ),
		'border_states'	=>	array( 'CA', 'NV', 'ID' ),
		'border_zips'	=>	array(	/* California */'955', '960', '976', '961',
									/* Nevada */	'894', 
									/* Idaho */		'838', '835', '836' )
	),
	'ID-MT-WY'	=>	array(
		'states'	=>	array( 'ID', 'MT', 'WY' ),
		'border_states'	=>	array( 'WA', 'OR', 'NV', 'UT', 'CO', 'NE', 'SD', 'ND' ),
		'border_zips'	=>	array(	/* Washington */'991', '990', '992',
									/* Oregon */	'978', '979', 
									/* Nevada */	'838', '835', '836',
									/* Utah */		'843', '840',
									/* Colorado */	'816', '804', '805', '806', '807', 
									/* Nebraska */	'693', '691',
									/* S. Dakota */	'577',
									/* N. Dakota */	'588', '586', '592' )
	),
	'AK'	=>	array(
		'states'	=>	array( 'AK' ),
		'border_states'	=>	array(),
		'border_zips'	=>	array()
	),
	'HI'	=>	array(
		'states'	=>	array( 'HI' ),
		'border_states'	=>	array(),
		'border_zips'	=>	array()
	)
);



?>